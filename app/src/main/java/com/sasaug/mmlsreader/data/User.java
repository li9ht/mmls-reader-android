package com.sasaug.mmlsreader.data;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sasaug.mmlsreader.MainApplication;
import com.sasaug.mmlsreader.core.objs.Announcement;
import com.sasaug.mmlsreader.core.objs.Subject;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 *
 * This class will maintain user login data on the device. It's persistent storage.
 *
 * Created by sasaug on 10/26/14.
 */

public class User {

    private static String username;
    private static String password;
    private static ArrayList<Subject> subjects;
    private static ArrayList<Announcement> announcements;

    private static SharedPreferences getPrefs(){
        return MainApplication.getContext().getSharedPreferences("user", Context.MODE_PRIVATE);
    }

    public static String getUsername() {
        if(username == null)
            username = getPrefs().getString("username", "");
        return username;
    }

    public static void setUsername(String value) {
        username = value;
        getPrefs().edit().putString("username", username).commit();
    }

    public static String getPassword() {
        if(password == null)
            password = getPrefs().getString("password", "");
        return password;
    }

    public static void setPassword(String value) {
        password = value;
        getPrefs().edit().putString("password", password).commit();
    }

    public static boolean getFetchOnLoad() {
        return getPrefs().getBoolean("fetchOnLoad", false);
    }

    public static void setFetchOnLoad(boolean value) {
        getPrefs().edit().putBoolean("fetchOnLoad", value).commit();
    }

    public static int getAutoSyncTime() {
        return getPrefs().getInt("autoSyncTime", 0);
    }

    public static void setAutoSyncTime(int value) {
        getPrefs().edit().putInt("autoSyncTime", value).commit();
    }

    public static ArrayList<Subject> getSubjects() {
        if(subjects == null) {
            String str = getPrefs().getString("subjects", "");
            Type listType = new TypeToken<ArrayList<Subject>>() {}.getType();
            subjects = new Gson().fromJson(str, listType);
            return subjects;
        }
        return subjects;
    }

    public static void setSubjects(ArrayList<Subject> values) {
        subjects = values;
        getPrefs().edit().putString("subjects", new Gson().toJson(subjects)).commit();
    }

    public static ArrayList<Announcement> getAnnouncements() {
        if(announcements == null) {
            String str = getPrefs().getString("announcements", "");
            Type listType = new TypeToken<ArrayList<Announcement>>() {}.getType();
            announcements = new Gson().fromJson(str, listType);
            return announcements;
        }
        return announcements;
    }

    public static void setAnnouncements(ArrayList<Announcement> values) {
        announcements = values;
        getPrefs().edit().putString("announcements", new Gson().toJson(announcements)).commit();
    }

    public static void clear(){
        username = null;
        password = null;
        subjects = null;
        announcements = null;
        getPrefs().edit().clear().commit();
    }
}
