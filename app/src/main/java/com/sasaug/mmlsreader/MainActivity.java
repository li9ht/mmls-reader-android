package com.sasaug.mmlsreader;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.transition.*;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sasaug.mmlsreader.core.Engine;
import com.sasaug.mmlsreader.core.EngineInterface;
import com.sasaug.mmlsreader.core.objs.Announcement;
import com.sasaug.mmlsreader.core.objs.File;
import com.sasaug.mmlsreader.core.objs.Subject;
import com.sasaug.mmlsreader.data.User;
import com.sasaug.mmlsreader.util.MaterialManager;
import com.sasaug.mmlsreader.util.views.RippleCardView;
import com.sasaug.mmlsreader.util.views.RippleImageView;
import com.sasaug.mmlsreader.util.views.RippleLinearLayout;
import com.sasaug.mmlsreader.util.views.RippleRelativeLayout;

import java.util.ArrayList;


public class MainActivity extends ActionBarActivity {

    static final String[] COLORS = {"#e51c23", "#9c27b0", "#3f51b5", "#03a9f4", "#259b24", "#795548", "#607d8b", "#009688"};


    Toolbar toolbar;
    RecyclerView recyclerView;
    CardView announcementView;
    RelativeLayout announcementLayout;
    View announcementBaseLayer;
    RelativeLayout content;
    ImageView fabSettings;
    View refreshView;

    //announcement view
    TextView mTitle;
    TextView mAuthor;
    TextView mDate;
    TextView mMsg;
    RippleImageView mBtnClose;



    ArrayList<Announcement> announcements = new ArrayList<Announcement>();
    ArrayList<Subject> subjects = new ArrayList<Subject>();

    LinearLayoutManager mLinearLayoutManager;
    SubjectAdapter mSubjectAdapter;

    int mLastX = -1;
    int mLastY = -1;

    boolean isSettingsOpen = false;

    class EngineRequest implements EngineInterface{

        public EngineRequest(){
            if(refreshView != null){
                refreshView.animate().rotationBy(120 * 360).setDuration(50000).setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        refreshView.animate().rotation(0);
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {
                        refreshView.animate().rotation(0);
                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });
            }
        }

        Handler handler = new Handler(new Handler.Callback()
        {
            @Override
            public boolean handleMessage(android.os.Message msg){
                if(mSubjectAdapter != null)
                    mSubjectAdapter.notifyDataSetChanged();
                refreshView.animate().cancel();
                return true;
            }
        });

        Handler errorHandler = new Handler(new Handler.Callback()
        {
            @Override
            public boolean handleMessage(android.os.Message msg){
                refreshView.animate().cancel();
                Toast.makeText(MainActivity.this, R.string.error_network, Toast.LENGTH_SHORT).show();
                return true;
            }
        });

        @Override
        public void onFetchData(boolean success, ArrayList<Announcement> anns, ArrayList<Subject> subs) {
            announcements.clear();
            announcements.addAll(anns);
            subjects.clear();
            subjects.addAll(subs);
            handler.sendEmptyMessage(0);
            mEngineRequest = null;
        }

        @Override
        public void onError() {
            mEngineRequest = null;
        }
    }

    EngineRequest mEngineRequest = null;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.clear();

        menu.add(R.string.refresh)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        final MenuItem refreshItem = menu.getItem(0);
        refreshView = LayoutInflater.from(MainActivity.this).inflate(R.layout.actionitem_refresh, null, false);
        refreshItem.setActionView(refreshView);

        refreshView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mEngineRequest == null) {
                    mEngineRequest = new EngineRequest();
                    Engine.fetchData(MainActivity.this, User.getUsername(), User.getPassword(), mEngineRequest);
                }
            }
        });

        if(mEngineRequest == null && User.getFetchOnLoad()) {
            mEngineRequest = new EngineRequest();
            Engine.fetchData(MainActivity.this, User.getUsername(), User.getPassword(), mEngineRequest);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed(){
        if(isSettingsOpen){
            isSettingsOpen = false;
            final Scene scene = Scene.getSceneForLayout(content, R.layout.scene_main_settings_close, MainActivity.this);
            TransitionManager.go(scene, new ChangeBounds());
            setupSettingScene(false, scene.getSceneRoot());
        }else if(announcementLayout.getVisibility() == View.VISIBLE){
            mBtnClose.performClick();
        }else{
            super.onBackPressed();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //setup the toolbar to replace actionbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.app_name));
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);

        //link up the view to code
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        announcementView = (CardView) findViewById(R.id.announcementView);
        announcementLayout = (RelativeLayout)findViewById(R.id.announcementLayout);
        announcementBaseLayer = findViewById(R.id.announcementBaseLayer);
        content = (RelativeLayout) findViewById(R.id.content);
        fabSettings = (ImageView)findViewById(R.id.fabSettings);
        mTitle = (TextView) findViewById(R.id.title);
        mAuthor = (TextView) findViewById(R.id.author);
        mDate = (TextView) findViewById(R.id.date);
        mMsg = (TextView) findViewById(R.id.msg);
        mBtnClose = (RippleImageView) findViewById(R.id.btnClose);

        //setup adapter
        mSubjectAdapter = new SubjectAdapter(User.getSubjects(), User.getAnnouncements());

        //setup recyclerview
        mLinearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLinearLayoutManager);
        recyclerView.setAdapter(mSubjectAdapter);

        //setup the close button
        mBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                announcementBaseLayer.setVisibility(View.GONE);
                ScaleAnimation animation = new ScaleAnimation(1.0f, 0.25f, 1.0f, 0.25f, mLastX, mLastY);
                animation.setDuration(500);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        announcementLayout.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });
                announcementLayout.startAnimation(animation);
            }
        });

        //let's setup the setting scene
        setupSettingScene(true, null);

        //add a scroll listener to recyclerView to hide/show our floating action button
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener(){
            int direction = 0;
            @Override
            public void onScrolled(RecyclerView view, int dx, int dy){
                //we try not to listen to too small pixel changes, as Android might sometimes throw
                //weird small values

                if(dy > 10) {    //this means the user is scrolling down
                    //this tell us what's out last moving direction, we only execute if previously
                    //we are not moving down. Without this, the animation will keep replay, noty
                    //something we want.
                    if(direction != 1){
                        direction = 1;
                        //load the sliding animation from our res/anim folder
                        //we hide the button once animation is done
                        Animation slide = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slidein_btm);
                        slide.setAnimationListener(new Animation.AnimationListener() {
                            public void onAnimationStart(Animation animation) {}
                            public void onAnimationEnd(Animation animation) {
                                fabSettings.setVisibility(View.GONE);
                            }
                            public void onAnimationRepeat(Animation animation) {}
                        });
                        fabSettings.startAnimation(slide);
                    }
                }else if(dy < -10){ //this means the user is scrolling up
                    if(direction != -1){
                        direction = -1;
                        //load the sliding animation from our res/anim folder
                        //we un-hide the button at start or else user can't see the animation
                        Animation slide = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slideout_btm);
                        slide.setAnimationListener(new Animation.AnimationListener() {
                            public void onAnimationStart(Animation animation) {
                                fabSettings.setVisibility(View.VISIBLE);
                            }
                            public void onAnimationEnd(Animation animation) {}
                            public void onAnimationRepeat(Animation animation) {}
                        });
                        fabSettings.startAnimation(slide);
                    }
                }
            }
        });

    }

    public void setupSettingScene(boolean onCreate, View v){

        if(!onCreate) {
            fabSettings = (ImageView) v.findViewById(R.id.fabSettings);
        }

        //This function is not working at the point of writing, maybe enable it later?
        //TODO: Enable this when Google fixed this support library
        //ViewCompat.setElevation(fabSettings, 5f);

        fabSettings.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                /* To open settings, we are using Transition API from Kitkat

                 * In Lollipop, you can even do it for Activity transition, but here we just open
                 * up a settings overlay.
                 *
                 * We use TransitionEverywhere library which allow us to do it for devices starting
                 * from ICS
                 */


                final Scene scene = Scene.getSceneForLayout(content, R.layout.scene_main_settings, MainActivity.this);
                isSettingsOpen = true;
                scene.setEnterAction(new Runnable() {
                    public void run() {
                        //setup the views within the settings scene
                        View row1 = scene.getSceneRoot().findViewById(R.id.row1);
                        View row2 = scene.getSceneRoot().findViewById(R.id.row2);
                        View row3 = scene.getSceneRoot().findViewById(R.id.row3);
                        View row4 = scene.getSceneRoot().findViewById(R.id.row4);
                        SwitchCompat switch1 = (SwitchCompat)scene.getSceneRoot().findViewById(R.id.switch1);
                        SwitchCompat switch2 = (SwitchCompat)scene.getSceneRoot().findViewById(R.id.switch2);

                        //We want the settings enter row by row, coming in from left, each with 200ms
                        //delay after another
                        Animation translateAnim1 = new TranslateAnimation(-100, 0, 0, 0);
                        translateAnim1.setDuration(300);
                        Animation alphaAnim1 = new AlphaAnimation(0, 1);
                        alphaAnim1.setDuration(500);
                        AnimationSet set1 = new AnimationSet(true);
                        set1.addAnimation(translateAnim1);
                        set1.addAnimation(alphaAnim1);
                        set1.setStartOffset(200);
                        row1.startAnimation(set1);

                        Animation translateAnim2 = new TranslateAnimation(-100, 0, 0, 0);
                        translateAnim2.setDuration(300);
                        Animation alphaAnim2 = new AlphaAnimation(0, 1);
                        alphaAnim2.setDuration(500);
                        AnimationSet set2 = new AnimationSet(true);
                        set2.addAnimation(translateAnim2);
                        set2.addAnimation(alphaAnim2);
                        set2.setStartOffset(400);
                        row2.startAnimation(set2);

                        Animation translateAnim3 = new TranslateAnimation(-100, 0, 0, 0);
                        translateAnim3.setDuration(300);
                        Animation alphaAnim3 = new AlphaAnimation(0, 1);
                        alphaAnim3.setDuration(500);
                        AnimationSet set3 = new AnimationSet(true);
                        set3.addAnimation(translateAnim3);
                        set3.addAnimation(alphaAnim3);
                        set3.setStartOffset(600);
                        row3.startAnimation(set3);

                        Animation translateAnim4 = new TranslateAnimation(-100, 0, 0, 0);
                        translateAnim4.setDuration(300);
                        Animation alphaAnim4 = new AlphaAnimation(0, 1);
                        alphaAnim4.setDuration(500);
                        AnimationSet set4 = new AnimationSet(true);
                        set4.addAnimation(translateAnim4);
                        set4.addAnimation(alphaAnim4);
                        set4.setStartOffset(800);
                        row4.startAnimation(set4);

                        //load the previous config
                        switch1.setChecked(User.getFetchOnLoad());
                        switch2.setChecked(User.getAutoSyncTime() != 0? true: false);

                        //add listener to detect the switch changes, we don't have ok or save button
                        //to save our settings so just save it as their value changes
                        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                User.setFetchOnLoad(b);
                            }
                        });

                        switch2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                if(b){
                                    Toast.makeText(MainActivity.this, R.string.settings_autosync_notavailable, Toast.LENGTH_LONG).show();
                                    User.setAutoSyncTime(60);
                                }else{
                                    User.setAutoSyncTime(0);
                                }
                            }
                        });

                        //About button, opens up our material design based dialog
                        row3.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                MaterialManager.MaterialDialog dialog= new MaterialManager.MaterialDialog();
                                dialog.setup(getString(R.string.settings_about), getString(R.string.settings_aboutmore), getString(R.string.settings_ok), null);
                                DialogFragment fragment = dialog;
                                fragment.show(getSupportFragmentManager(), "dialog");
                            }
                        });

                        //Log out button
                        row4.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                MaterialManager.MaterialDialog dialog= new MaterialManager.MaterialDialog();
                                dialog.setup(getString(R.string.settings_logout), getString(R.string.settings_logoutmore), getString(R.string.settings_yes), getString(R.string.settings_no));
                                dialog.setListener(new MaterialManager.MaterialDialog.Listener() {
                                    @Override
                                    public void onButtonOkPressed() {
                                        //delete all User detail
                                        User.clear();

                                        //close this activity and reopen Login!
                                        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }

                                    @Override
                                    public void onButtonCancelPressed() {}
                                });
                                DialogFragment fragment = dialog;
                                fragment.show(getSupportFragmentManager(), "dialog");
                            }
                        });
                    }
                });
                TransitionManager.go(scene, new ChangeBounds().setDuration(300));
            }
        });

    }

    public class SubjectAdapter extends RecyclerView.Adapter<SubjectAdapter.ViewHolder> {
        ArrayList<Subject> list;
        ArrayList<Announcement> announcements;

        public class ViewHolder extends RecyclerView.ViewHolder{

            private int mOriginalHeight = 0;
            private boolean mIsViewExpanded = false;

            View v;
            TextView title;
            LinearLayout extension;
            RippleImageView btnArchive;
            ImageView btnExpand;

            public ViewHolder(View v) {
                super(v);
                this.v = v;
                ((RippleLinearLayout)v).getRippleHelper().setColor(getResources().getColor(R.color.litegrey));
                title = (TextView) v.findViewById(R.id.title);
                extension = (LinearLayout) v.findViewById(R.id.extension);
                btnArchive = (RippleImageView) v.findViewById(R.id.btnArchive);
                btnArchive.getRippleHelper().setColor(getResources().getColor(R.color.litegrey));
                btnExpand = (ImageView) v.findViewById(R.id.btnExpand);
            }

            public void animateClick(int height){
                if (mOriginalHeight == 0) {
                    mOriginalHeight = v.getHeight();
                }
                ValueAnimator valueAnimator;
                if (!mIsViewExpanded) {
                    btnExpand.animate().rotation(180f);
                    mIsViewExpanded = true;
                    valueAnimator = ValueAnimator.ofInt(mOriginalHeight, mOriginalHeight + height);
                } else {
                    btnExpand.animate().rotation(0f);
                    mIsViewExpanded = false;
                    valueAnimator = ValueAnimator.ofInt(mOriginalHeight + height, mOriginalHeight);
                }
                valueAnimator.setDuration(200);
                valueAnimator.setInterpolator(new LinearInterpolator());
                valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    public void onAnimationUpdate(ValueAnimator animation) {
                        Integer value = (Integer) animation.getAnimatedValue();
                        v.getLayoutParams().height = value.intValue();
                        v.requestLayout();
                    }
                });
                valueAnimator.start();

            }
        }

        @Override
        public SubjectAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(MainActivity.this).inflate(R.layout.item_subject, parent, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        //usually, we only keep 1 list in an adapter, because announcements should be arranged into
        //subjects but oh well, I'm lazy :P
        public SubjectAdapter(ArrayList<Subject> list, ArrayList<Announcement> announcements) {
            this.list = list;
            this.announcements = announcements;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            final Subject subject = list.get(position);

            holder.title.setText(subject.code + " - " + subject.name);

            holder.btnArchive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MaterialRecyclerViewDialog dialog= new MaterialRecyclerViewDialog();
                    dialog.setup(subject.files, subject.alllecturedownloads, subject.alltutorialdownloads);
                    DialogFragment fragment = dialog;
                    fragment.show(getSupportFragmentManager(), "dialog");
                }
            });

            /*
            *   Why postDelayed? We delay the announcements from being created after 100ms of the
            *   subject in the list being created because user usually don't see these announcements
            *   as they are still in their hidden state.
            *
            *   If we don't put postDelayed, this code will be executed when creating the list, wasting
            *   precious main thread time. We let the view initialised then wait 100ms, then start
            *   adding these subviews into the list. This way, user won't feel laggy when starting
            *   the app up.
            *
            * */
            holder.v.postDelayed(new Runnable(){
                public void run(){
                    holder.extension.removeAllViews();

                    for(int i = 0 ; i < announcements.size(); i++) {
                        final Announcement announcement = announcements.get(i);
                        if(announcement.subject.equals(subject.code)) {
                            final View v = LayoutInflater.from(MainActivity.this).inflate(R.layout.item_subject_ext, null, false);
                            final RippleCardView card= (RippleCardView) v.findViewById(R.id.card);
                            TextView subTitle = (TextView) v.findViewById(R.id.title);
                            TextView subDate = (TextView) v.findViewById(R.id.date);
                            TextView subAuthor = (TextView) v.findViewById(R.id.author);

                            card.getRippleHelper().setColor(getResources().getColor(R.color.litegrey) );

                            subTitle.setText(announcement.title);
                            subDate.setText(announcement.date);
                            subAuthor.setText(announcement.author);

                            card.setOnTouchListener(new View.OnTouchListener() {
                                @Override
                                public boolean onTouch(View v, MotionEvent event) {
                                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                                        Point p = new Point((int)event.getX(), (int)event.getY());
                                        card.setTag(p);
                                    }
                                    //required workaround if you had override onTouch on the view or
                                    //else ripple wont work since they rely on this as well
                                    card.getRippleHelper().onTouchEvent(event);
                                    return v.onTouchEvent(event);
                                }
                            });

                            card.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    mTitle.setText(announcement.title);
                                    mAuthor.setText(announcement.author);
                                    mDate.setText(announcement.date);
                                    mMsg.setText(announcement.msg);

                                    Point p = (Point)card.getTag();
                                    int[] mLastLocation = new int[2];
                                    card.getLocationInWindow (mLastLocation);
                                    int y = (int)(mLastLocation[1]-toolbar.getHeight());
                                    y += p.y;
                                    mLastX = p.x;
                                    mLastY = y;

                                    announcementLayout.setVisibility(View.VISIBLE);
                                    ScaleAnimation animation = new ScaleAnimation(0.25f, 1.0f, 0.25f, 1.0f, p.x, y);
                                    animation.setDuration(500);
                                    animation.setAnimationListener(new Animation.AnimationListener() {
                                        @Override
                                        public void onAnimationStart(Animation animation) {

                                            announcementBaseLayer.setVisibility(View.GONE);
                                        }

                                        @Override
                                        public void onAnimationEnd(Animation animation) {
                                            announcementBaseLayer.setVisibility(View.VISIBLE);
                                        }

                                        @Override
                                        public void onAnimationRepeat(Animation animation) {}
                                });
                                announcementLayout.startAnimation(animation);



                                }
                            });
                            holder.extension.addView(v);
                        }
                    }
                }
            }, 100);

            holder.v.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    holder.extension.setVisibility(View.VISIBLE);
                    int height = (int)(holder.extension.getChildCount() * getResources().getDisplayMetrics().density * 72);
                    holder.animateClick(height);
                }
            });
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }

    public static class MaterialRecyclerViewDialog extends DialogFragment {
        ArrayList<File> list;
        String lecture;
        String tutorial;

        public void setup(ArrayList<File> list, String lecture, String tutorial){
            this.list = list;
            this.lecture = lecture;
            this.tutorial = tutorial;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);

            View v = inflater.inflate(R.layout.dialog_material_recyclerview, container, false);

            final RecyclerView rv = (RecyclerView) v.findViewById(R.id.list);

            final FilesAdapter adapter = new FilesAdapter(list, lecture, tutorial);

            LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getActivity());
            rv.setLayoutManager(mLinearLayoutManager);
            rv.setAdapter(adapter);

            rv.post(new Runnable() {
                public void run() {
                    int height = (int) (adapter.getItemCount() * (56 * getResources().getDisplayMetrics().density));
                    if (rv.getHeight() > height) {
                        ViewGroup.LayoutParams param = rv.getLayoutParams();
                        param.height = height;
                        rv.setLayoutParams(param);
                    }

                }
            });

            return v;
        }

        public class FilesAdapter extends RecyclerView.Adapter<FilesAdapter.ViewHolder> {
            ArrayList<File> list;
            String lecture;
            String tutorial;

            public class ViewHolder extends RecyclerView.ViewHolder{
                View v;
                TextView title;

                public ViewHolder(View v) {
                    super(v);
                    this.v = v;
                    ((RippleRelativeLayout)v).getRippleHelper().setColor(getResources().getColor(R.color.litegrey));
                    title = (TextView) v.findViewById(R.id.title);
                }
            }

            @Override
            public FilesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View v = LayoutInflater.from(getActivity()).inflate(R.layout.item_file, parent, false);
                ViewHolder vh = new ViewHolder(v);
                return vh;
            }

            public FilesAdapter(ArrayList<File> list, String lecture, String tutorial) {
                this.list = list;
                this.lecture = lecture;
                this.tutorial = tutorial;
            }

            @Override
            public void onBindViewHolder(final ViewHolder holder, final int position) {
                if(position == 0){  //all lecturer notes
                    holder.title.setText(R.string.alllectnotes);
                    holder.v.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String url = lecture;
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }
                    });
                }else if(position == 1){
                    holder.title.setText(R.string.alltutnotes);
                    holder.v.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String url = tutorial;
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }
                    });
                }else{
                    final File file = list.get(position-2);
                    holder.title.setText(file.name);
                    holder.v.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String url = file.url;
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }
                    });
                }


            }

            @Override
            public int getItemCount() {
                if(list == null)
                    return 2;
                return list.size() + 2;
            }
        }
    }



}
