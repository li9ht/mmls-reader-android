package com.sasaug.mmlsreader.core;

import android.content.Context;

import com.google.gson.Gson;
import com.sasaug.mmlsreader.R;
import com.sasaug.mmlsreader.core.objs.Announcement;
import com.sasaug.mmlsreader.core.objs.Subject;
import com.sasaug.mmlsreader.data.User;
import com.sasaug.network.common.NetManager;
import com.sasaug.network.common.WebRequestAdapter;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * This file manage all the backend work to login MMLS and fetching data from it
 *
 * Created by sasaug on 10/26/14.
 */

public class Engine {

    //the url to php which helps to parse MMLS and return result in JSON
    private final static String URL = "http://mmlsreaderweb.appspot.com/mmls.php";

    //fetch data which will fetch data from the URL above using the user's MMLS login detail and return result via EngineInterface callback
    public static void fetchData(Context context, final String username, final String password, final EngineInterface callback) {

        //check if it's demo login
        if(username.equals(context.getString(R.string.demo_username)) &&
                password.equals(context.getString(R.string.demo_password))) {

            //create a thread to mimic network call
            class FakeThread extends Thread{
                public void run(){
                    try {
                        Thread.sleep(2500);
                    }catch(Exception ex){}

                    FetchResponse result = generateDemoData();
                    User.setSubjects(result.subjects);
                    User.setAnnouncements(result.announcements);
                    callback.onFetchData(result.isSuccess(), result.announcements, result.subjects);

                }

            }
            new FakeThread().start();

        }else{
            class WebRequest extends WebRequestAdapter {
                public void onGetResult(String response) {
                    FetchResponse result = new Gson().fromJson(response, FetchResponse.class);

                    //now we have already fetch result, if it's a success, lets store the data before returning it back via the callback
                    if (result.isSuccess()) {
                        User.setSubjects(result.subjects);
                        User.setAnnouncements(result.announcements);
                    }

                    callback.onFetchData(result.isSuccess(), result.announcements, result.subjects);
                }

                public void onError(Exception ex) {
                    callback.onError();
                }

            }
            List<NameValuePair> data = new ArrayList<NameValuePair>();
            data.add(new BasicNameValuePair("u", username));
            data.add(new BasicNameValuePair("p", password));
            NetManager.postHtml(URL, data, new WebRequest());
        }
    }

    private static FetchResponse generateDemoData(){
        FetchResponse res = new FetchResponse();

        res.success = 1;
        res.announcements = new ArrayList<Announcement>();
        res.subjects = new ArrayList<Subject>();

        for(int i = 0; i < 20; i++){
            Announcement ann = new Announcement();
            ann.subject = "TCP" + i%4;
            ann.author = "Professor " + i%5 + i%5;
            ann.date = (30-i) + "-OCT-2014";
            ann.msg = "This is just a demo announcements.\n\nI say tomorrow exam, OK?";
            ann.title = "Title " + i;
            res.announcements.add(ann);
        }

        for(int i = 0; i < 4; i++){
            Subject sub = new Subject();
            sub.code = "TCP" + i%4;
            sub.alllecturedownloads = "http://www.android.com//new/images/logos-2x/android-wordmark-8EC047.png";
            sub.alltutorialdownloads = "http://www.android.com//new/images/logos-2x/android-wordmark-8EC047.png";
            sub.name = "Subject " + i;
            sub.semId = "56";
            res.subjects.add(sub);
        }


        return res;
    }
}
