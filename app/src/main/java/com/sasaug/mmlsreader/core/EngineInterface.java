package com.sasaug.mmlsreader.core;

import com.sasaug.mmlsreader.core.objs.Announcement;
import com.sasaug.mmlsreader.core.objs.Subject;

import java.util.ArrayList;

/**
 * Created by sasaug on 10/26/14.
 */
public interface EngineInterface {
    public void onFetchData(boolean success, ArrayList<Announcement> announcements, ArrayList<Subject> subjects);
    public void onError();
}
