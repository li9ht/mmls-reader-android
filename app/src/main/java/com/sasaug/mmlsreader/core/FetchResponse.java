package com.sasaug.mmlsreader.core;

import com.sasaug.mmlsreader.core.objs.Announcement;
import com.sasaug.mmlsreader.core.objs.Subject;

import java.util.ArrayList;

/**
 * Created by sasaug on 10/26/14.
 */
public class FetchResponse {
    public int success = 0;
    public ArrayList<Announcement> announcements;
    public ArrayList<Subject> subjects;

    public boolean isSuccess(){
        return success == 1;
    }
}
