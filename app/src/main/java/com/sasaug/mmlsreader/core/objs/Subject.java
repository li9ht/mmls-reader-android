package com.sasaug.mmlsreader.core.objs;

import java.util.ArrayList;

/**
 * Created by sasaug on 10/26/14.
 */
public class Subject {
    public String semId;
    public String name;
    public String code;
    public ArrayList<File> files;
    public int files_count;
    public String alllecturedownloads;
    public String alltutorialdownloads;
}
