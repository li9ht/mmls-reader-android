package com.sasaug.network.common;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetManager {
	
	public static final String TAG_GET = "GET"; 
	public static final String TAG_POST = "POST";
	public static final String TAG_PUT = "PUT";
	public static final String TAG_GET_STREAM = "GET_STREAM"; 
	public static final String TAG_POST_STREAM = "POST_STREAM"; 
	public static final String TAG_GET_BINARY = "GET_BINARY";
	public static final String TAG_POST_BINARY = "POST_BINARY";
	public static final String TAG_POST_STRING = "POST_STRING";
	
	public static final String ERROR_NOINTERNET = "NO_INTERNET";
	
	
	public static ArrayList<NetTask> tasks = new ArrayList<NetTask>();
	public static ArrayList<NetGroup> groups = new ArrayList<NetGroup>();
	public static ExecutorService pool = Executors.newFixedThreadPool(10);
	
	private static Context context = null;
	
	//Set the context, don't set this or set it to null, if you don't want the library to verify for connectivity when performing calls.
	public static void setContext(Context c)
	{
		context = c;
	}
	
	//Set the amount of threads available to the library. This does not include threaded download progress feature due to different nature.
	public static void setPoolThreads(int size)
	{
		if(size <= 0 )
			return;
		pool.shutdown();
		pool = Executors.newFixedThreadPool(size);
	}
	
	//Set the read timeout, default at 60 seconds
	public static void setReadTimeout(int timeout)
	{
		WebCall.READ_TIMEOUT = timeout;
	}
	
	//Set the connection timeout, default at 10 seconds
	public static void setConnectTimeout(int timeout)
	{
		WebCall.TIMEOUT = timeout;
	}
	
	//Set whether to perform download progress update in seperate thread due to burst callback 
	public static void setUseThreadedDownloadProgress(boolean value)
	{
		WebCallAdapter.threadedDownloadProgressCallback = true;
	}
	
	//Kill all running task within the library
	public static void killAllTask()
	{
		for(int i =0; i < tasks.size();i++)
		{
			try
			{
				NetTask task = tasks.get(i);
				task.killTask();
				removeTask(task.pid);
			}
			catch(Exception ex)
			{
				
			}
		}
	}
	
	//Kill task based on their tag, tag refer to the call type like GET/POST etc
	public static void killTask(String tag)
	{
		for(int i =0; i < tasks.size();i++)
		{
			try
			{
				NetTask task = tasks.get(i);
				if(task.tag.equals(tag))
				{
					task.killTask();
					removeTask(task.pid);
				}
			}catch(Exception ex)
			{
				
			}
				
		}
	}
	
	//Kill task based on their id
	public static void killTask(int id)
	{
		for(int i =0; i < tasks.size();i++)
		{
			try
			{
				NetTask task = tasks.get(i);
				if(task.pid == id)
				{
					task.killTask();
					removeTask(task.pid);
				}
			}catch(Exception ex)
			{
		
			}
				
		}
	}
	
	//Remove task based on their id, this will not kill it if it's running
	public static void removeTask(int id)
	{
		for(int i =0; i < tasks.size();i++)
		{
			try
			{
				NetTask task = tasks.get(i);
				if(task.pid == id)
				{
					tasks.remove(i);
				}
			}catch(Exception ex)
			{
	
			}
		}
	}

	// Obtain a web page using GET method. Callback used: onGetResult(String)
	public static int getHtml(String url, WebRequestIFace request)
	{
		if(context != null)
		{
			if(!isOnline(context))
			{
				request.onError(new Exception(ERROR_NOINTERNET));
				request.onError(ERROR_NOINTERNET);
				return 0;
			}
		}
		WebCall call;
		if(request.setAuthUsername() == null && request.setAuthPassword() == null)
			call = new WebCall(new GetURLTick(request),url, WebCall.METHOD_GET);
		else
			call = new WebCall(new GetURLTick(request),url, WebCall.METHOD_GET, request.setAuthUsername(), request.setAuthPassword());
		NetTask task = new NetTask(call.pid, TAG_GET, call);
		tasks.add(task);
		pool.execute(call);
		return call.pid;
	}
	
	// Obtain a web page using GET method. Callback used: onGetResult(InputStream)
	public static int getHtmlStream(String url, WebRequestIFace request)
	{
		if(context != null)
		{
			if(!isOnline(context))
			{
				request.onError(new Exception(ERROR_NOINTERNET));
				request.onError(ERROR_NOINTERNET);
				return 0;
			}
		}
		WebCall call;
		if(request.setAuthUsername() == null && request.setAuthPassword() == null)
			call = new WebCall(new GetURLTick(request),url,  WebCall.METHOD_GET_STREAM);
		else
			call = new WebCall(new GetURLTick(request),url,  WebCall.METHOD_GET_STREAM);
		NetTask task = new NetTask(call.pid, TAG_GET_STREAM, call);
		tasks.add(task);
		pool.execute(call);
		return call.pid;
	}

	// Obtain web pages in parallel but callback is done 1 after another according to the order it is inserted 
	public static void getHtmlGroup(ArrayList<String> url, ArrayList<WebRequestIFace> request)
	{
		if(context != null)
		{
			if(!isOnline(context))
			{
				for(int i = 0; i < request.size(); i++)
				{
					request.get(i).onError(ERROR_NOINTERNET);
					request.get(i).onError(new Exception(ERROR_NOINTERNET));
				}
				return;
			}
		}
		final NetGroup group = new NetGroup();
		class FakeCallback extends WebRequestAdapter
		{
			int id =0;
			public FakeCallback(int id)
			{
				this.id = id;
			}
			
			public void onGetResult(String json)
			{
				
				boolean run = false;
				group.result.add(json);
				group.result_id.add(id);
				//Critical section
				synchronized(this) {
					group.taskDone++;	
				}
				
				if(!group.casted && group.taskDone == group.request.size())
				{
					run =true;
					group.casted = true;
				}
				
				if(run)
				{
					//Perform original callback 1 by 1, based on index value
					for(int i=0; i < group.request.size();i++)
					{
						//scan for correct id
						for(int j=0; j < group.result.size();j++)
						{
							if(group.result_id.get(j) == i)
							{
								group.request.get(i).onGetResult(group.result.get(j));
							}							
						}
						
					}
				}
			}
		}

		for(int i =0; i < request.size();i++)
		{
			//Store the original request to group
			group.request.add(request.get(i));
			
			//Create a fake callback
			WebRequestIFace callback = new FakeCallback(i);
			getHtml(url.get(i), callback);
		}	
	}
	
	// Obtain a web page using POST method and place parameters into POST content. Callback used: onGetResult(String)
	public static int postHtml(String url, List<NameValuePair> data, WebRequestIFace request)
	{
		if(context != null)
		{
			if(!isOnline(context))
			{
				request.onError(ERROR_NOINTERNET);
				request.onError(new Exception(ERROR_NOINTERNET));
				return 0;
			}
		}
		WebCall call;
		if(request.setAuthUsername() == null && request.setAuthPassword() == null)
			call = new WebCall(new GetURLTick(request),url, WebCall.METHOD_POST, data);
		else 
			call = new WebCall(new GetURLTick(request),url, WebCall.METHOD_POST, data, request.setAuthUsername(), request.setAuthPassword());
	    NetTask task = new NetTask(call.pid, TAG_POST, call);
	    tasks.add(task);
	    pool.execute(call);
	    return call.pid;
	}
	
	// Obtain a web page using POST method and place String into the POST content. Callback used: onGetResult(String)
	public static int postHtmlString(String url, String data, WebRequestIFace request)
	{
		if(context != null)
		{
			if(!isOnline(context))
			{
				request.onError(ERROR_NOINTERNET);
				request.onError(new Exception(ERROR_NOINTERNET));
				return 0;
			}
		}
		WebCall call;
		if(request.setAuthUsername() == null && request.setAuthPassword() == null)
			call = new WebCall(new GetURLTick(request), url, WebCall.METHOD_POST_STRING, data);
		else
			call = new WebCall(new GetURLTick(request), url, WebCall.METHOD_POST_STRING, data, request.setAuthUsername(),request.setAuthPassword());
		
	    NetTask task = new NetTask(call.pid, TAG_POST_STRING, call);
	    tasks.add(task);
	    pool.execute(call);
	    return call.pid;
	}
	
	// Obtain a web page using POST method and place String into the POST content. Callback used: onGetResult(String)
	public static int postHtmlStream(String url, List<NameValuePair> data, WebRequestIFace request)
	{
		if(context != null)
		{
			if(!isOnline(context))
			{
				request.onError(ERROR_NOINTERNET);
				request.onError(new Exception(ERROR_NOINTERNET));
				return 0;
			}
		}
		WebCall call;
		if(request.setAuthUsername() == null && request.setAuthPassword() == null)
			call = new WebCall(new GetURLTick(request),url, WebCall.METHOD_POST_STREAM, data);
		else 
			call = new WebCall(new GetURLTick(request),url, WebCall.METHOD_POST_STREAM, data, request.setAuthUsername(), request.setAuthPassword());
	    NetTask task = new NetTask(call.pid, TAG_POST_STREAM, call);
	    tasks.add(task);
	    pool.execute(call);
	    return call.pid;
	}
	
	// Obtain web pages using POST method and place parameters into the POST content but callback is done 1 after another according to the order it is inserted 
	public static void postHtmlGroup(ArrayList<String> url, ArrayList<List<NameValuePair>> parameters, ArrayList<WebRequestIFace> request)
	{
		if(context != null)
		{
			if(!isOnline(context))
			{
				for(int i = 0; i < request.size(); i++)
				{
					request.get(i).onError(ERROR_NOINTERNET);
					request.get(i).onError(new Exception(ERROR_NOINTERNET));
				}
				return;
			}
		}
		final NetGroup group = new NetGroup();
		class FakeCallback extends WebRequestAdapter
		{
			int id =0;
			public FakeCallback(int id)
			{
				this.id = id;
			}
			
			public void onGetResult(String json)
			{
				
				boolean run = false;
				group.result.add(json);
				group.result_id.add(id);
				//Critical section
				synchronized(this) {
					group.taskDone++;	
				}
				
				if(!group.casted && group.taskDone == group.request.size())
				{
					run =true;
					group.casted = true;
				}
				
				if(run)
				{
					//Perform original callback 1 by 1, based on index value
					for(int i=0; i < group.request.size();i++)
					{
						//scan for correct id
						for(int j=0; j < group.result.size();j++)
						{
							if(group.result_id.get(j) == i)
							{
								group.request.get(i).onGetResult(group.result.get(j));
							}							
						}
						
					}
				}
			}
		}

		for(int i =0; i < request.size();i++)
		{
			//Store the original request to group
			group.request.add(request.get(i));
			
			//Create a fake callback
			WebRequestIFace callback = new FakeCallback(i);
			
			postHtml(url.get(i), parameters.get(i), callback);
		}	
	}
	
	// Obtain binary data from URL using GET method. If repeat is disabled, it will hook current callback to existing running callback.
	public static synchronized int getBinary(String url, WebRequestIFace request, boolean repeat)
	{
		if(context != null)
		{
			if(!isOnline(context))
			{
				request.onError(ERROR_NOINTERNET);
				request.onError(new Exception(ERROR_NOINTERNET));
				return 0;
			}
		}
		synchronized(tasks)
		{
			if(!repeat)
			{
				for(int i = 0; i < tasks.size(); i++)
				{
					if(tasks.get(i).task.url.equals(url) && tasks.get(i).task.state == WebCall.STATE_RUNNING)
					{
						tasks.get(i).task.callbacks.add(new GetURLTick(request));
						return 0;
					}
				}
			}
		}
		WebCall call;
		if(request.setAuthUsername() == null && request.setAuthPassword() == null)
			call = new WebCall(new GetURLTick(request),url, WebCall.METHOD_GET_BINARY);
		else 
			call = new WebCall(new GetURLTick(request),url, WebCall.METHOD_GET_BINARY, request.setAuthUsername(), request.setAuthPassword());
		NetTask task = new NetTask(call.pid, TAG_GET_BINARY, call);
		tasks.add(task);
		pool.execute(call);
		return call.pid;
	}
	
	// Obtain binary data from URL using POST method with parameters. If repeat is disabled, it will hook current callback to existing running callback.
	public static synchronized int postBinary(String url,  ArrayList<List<NameValuePair>> parameters, WebRequestIFace request, boolean repeat)
	{
		if(context != null)
		{
			if(!isOnline(context))
			{
				request.onError(ERROR_NOINTERNET);
				request.onError(new Exception(ERROR_NOINTERNET));
				return 0;
			}
		}
		
		synchronized(tasks)
		{
			if(!repeat)
			{
				for(int i = 0; i < tasks.size(); i++)
				{
					if(tasks.get(i).task.url.equals(url) && tasks.get(i).task.state == WebCall.STATE_RUNNING)
					{
						tasks.get(i).task.callbacks.add(new GetURLTick(request));
						return 0;
					}
				}
			}
		}
		
		WebCall call;
		if(request.setAuthUsername() == null && request.setAuthPassword() == null)
			call = new WebCall(new GetURLTick(request),url, WebCall.METHOD_POST_BINARY);
		else
			call = new WebCall(new GetURLTick(request),url, WebCall.METHOD_POST_BINARY, request.setAuthUsername(), request.setAuthPassword());
		NetTask task = new NetTask(call.pid, TAG_POST_BINARY, call);
		tasks.add(task);
		pool.execute(call);
		return call.pid;
	}
	
	// Obtain binary data in parallel but callback is done in order 1 after another
	public static void getBinaryGroup(ArrayList<String> url,ArrayList<WebRequestIFace> request)
	{
		if(context != null)
		{
			if(!isOnline(context))
			{
				for(int i = 0; i < request.size(); i++)
				{
					request.get(i).onError(ERROR_NOINTERNET);
					request.get(i).onError(new Exception(ERROR_NOINTERNET));
				}
				return;
			}
		}
		final NetGroup group = new NetGroup();
		class FakeCallback extends WebRequestAdapter
		{
			int id =0;
			public FakeCallback(int id)
			{
				this.id = id;
			}
			
			public void onGetResult(byte[] data)
			{
				
				boolean run = false;
				group.result_data.add(data);
				group.result_id.add(id);
				//Critical section
				synchronized(this) {
					group.taskDone++;	
				}
				
				if(!group.casted && group.taskDone == group.request.size())
				{
					run =true;
					group.casted = true;
				}
				
				if(run)
				{
					//Perform original callback 1 by 1, based on index value
					for(int i=0; i < group.request.size();i++)
					{
						//scan for correct id
						for(int j=0; j < group.result.size();j++)
						{
							if(group.result_id.get(j) == i)
							{
								group.request.get(i).onGetResult(group.result_data.get(j));
							}							
						}
						
					}
				}
			}
		}

		for(int i =0; i < request.size();i++)
		{
			//Store the original request to group
			group.request.add(request.get(i));
			
			//Create a fake callback
			WebRequestIFace callback = new FakeCallback(i);
			
			getBinary(url.get(i), callback, true);
		}	
	}

	// Obtain a web page using PUT method.
	public static int putHtml(String url, String json, WebRequestIFace request)
	{
		if(context != null)
		{
			if(!isOnline(context))
			{
				request.onError(ERROR_NOINTERNET);
				request.onError(new Exception(ERROR_NOINTERNET));
				return 0;
			}
		}
		WebCall call;
		if(request.setAuthUsername() == null && request.setAuthPassword() == null)
			call = new WebCall(new GetURLTick(request), url, json, WebCall.METHOD_PUT);
		else
			call = new WebCall(new GetURLTick(request), url, json, WebCall.METHOD_PUT, request.setAuthUsername(), request.setAuthPassword());
	    NetTask task = new NetTask(call.pid, TAG_PUT, call);
	    tasks.add(task);
	    pool.execute(call);
	    return call.pid;
	}

	// Process all the callbacks within the library
	protected static class GetURLTick extends WebCallAdapter {
		WebRequestIFace request;
		String url;
		
		GetURLTick(WebRequestIFace request)
		{
			this.request = request;
		}
		
		GetURLTick(WebRequestIFace request, String url)
		{
			this.request = request;
			this.url = url;
		}
		
		public ArrayList<NameValue> setHeaders()
		{
			return request.setHeaders();
		}
		
		public void onComplete(int pid, final String result, DefaultHttpClient client, final HttpResponse response)
		{
			removeTask(pid);
			request.onComplete(response, client);
			request.onGetResult(result);	
			request.onGetResult(result, response.getStatusLine());
		}
		
		public void onComplete(int pid, final byte[] result, DefaultHttpClient client, final HttpResponse response)
		{
			removeTask(pid);
			request.onComplete(response, client);
			request.onGetResult(result);
			request.onGetResult(result, response.getStatusLine());
		}
		
		public void onComplete(int pid, final InputStream result, DefaultHttpClient client, final HttpResponse response)
		{
			removeTask(pid);
			request.onComplete(response, client);
			request.onGetResult(result);	
			request.onGetResult(result, response.getStatusLine());
		}
		
		public void onError(int pid, Exception error)
		{
			removeTask(pid);
			request.onError(error);
			request.onError(error.getMessage());
		}
		
		public void onRedirected(String location)
		{
			request.onRedirected(location);
		}
		
		public void onDownloadProgressChange(long downloaded, long total)
		{
			request.onDownloadProgressChange(downloaded, total);
		}
		
		public boolean setRedirectHandler()
		{
			return request.setHandleRedirect();
		}
	}
	
	// Check if device is online
	//Utilities
	public static boolean isOnline(Context context)
	{
		try
    	{
			ConnectivityManager conMgr =  (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mobile = conMgr.getNetworkInfo(0);
			NetworkInfo wifi = conMgr.getNetworkInfo(1);
			if(wifi != null)
			{
				if(wifi.getState() == NetworkInfo.State.CONNECTED)
					return true;
			}
			if(mobile != null)
			{
				if(mobile.getState() == NetworkInfo.State.CONNECTED || mobile.getState() == NetworkInfo.State.CONNECTING)
					return true;
			}
    	}catch(Exception ex){}
    	return false;
	}
	
	// Check if device is connected to Internet
	public static boolean isWiFiConnected(Context context)
	{
		try
    	{
			ConnectivityManager conMgr =  (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo wifi = conMgr.getNetworkInfo(1);
			if(wifi != null)
			{
				if(wifi.getState() == NetworkInfo.State.CONNECTED)
					return true;
			}
    	}catch(Exception ex){}
    	return false;
	}
	
	
	public static boolean verifyHost(String url1, String url2)
	{
		try {
			URL uri1 = new URL(url1);
			URL uri2 = new URL(url2);
			if(uri1.getHost().equals(uri2.getHost()))
				return true;
			
		} catch (MalformedURLException e) {}
		return false;
	}
	
}


