package com.sasaug.network.common;

public interface NetLog {
	public void logDebug(String message);
	public void logError(String message);
}
