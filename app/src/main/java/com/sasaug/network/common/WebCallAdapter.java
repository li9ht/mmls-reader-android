package com.sasaug.network.common;

import java.io.InputStream;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.impl.client.DefaultHttpClient;

public class WebCallAdapter implements WebCallIFace{
	protected static boolean threadedDownloadProgressCallback = false;
	
	public void onRedirected(String newUrl) {}

	public void onError(int pid, Exception error) {}

	public void onComplete(int pid, DefaultHttpClient client, HttpResponse response) {}

	public void onComplete(int pid, String result, DefaultHttpClient client, HttpResponse response) {}

	public void onComplete(int pid, byte[] result, DefaultHttpClient client, HttpResponse response) {}

	public void onComplete(int pid, InputStream stream, DefaultHttpClient client, HttpResponse response) {}
	
	public void onDownloadProgressChange(long downloaded, long total) {}

	public DefaultHttpClient setDefaultHttpClient() {return null;}

	public ArrayList<NameValue> setHeaders(){return new ArrayList<NameValue>();}

	public final boolean useThreadedDownloadProgressCallback() {return threadedDownloadProgressCallback;}

	public boolean setRedirectHandler() { return true;}
}
