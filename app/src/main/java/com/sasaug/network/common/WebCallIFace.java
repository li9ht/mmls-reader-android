package com.sasaug.network.common;

import java.io.InputStream;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.impl.client.DefaultHttpClient;

public interface WebCallIFace {
		void onComplete(int pid, String result, DefaultHttpClient client, HttpResponse response);
		void onComplete(int pid, byte[] result, DefaultHttpClient client, HttpResponse response);
		void onComplete(int pid, InputStream stream, DefaultHttpClient client, HttpResponse response);
		void onError(int pid, Exception error);
		void onRedirected(String newUrl);
		void onDownloadProgressChange(long downloaded, long total);
	
		ArrayList<NameValue> setHeaders();
		boolean setRedirectHandler();
		DefaultHttpClient setDefaultHttpClient();
		boolean useThreadedDownloadProgressCallback();
}
