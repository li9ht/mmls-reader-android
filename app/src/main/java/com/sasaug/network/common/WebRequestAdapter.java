package com.sasaug.network.common;
import java.io.InputStream;
import java.util.ArrayList;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.impl.client.DefaultHttpClient;

public class WebRequestAdapter implements WebRequestIFace{	
	
	HttpResponse response;
	DefaultHttpClient client;
	
	//overrides
	public void onComplete(HttpResponse response, DefaultHttpClient client){this.response = response;this.client = client;}
	public void onGetResult(String result){}
	public void onGetResult(byte[] result){}
	public void onGetResult(InputStream result){}
	public void onGetResult(String result, StatusLine status){}
	public void onGetResult(byte[] result, StatusLine status){}
	public void onGetResult(InputStream result, StatusLine status){}	
	public void onError(String error){}
	public void onAllResultDone(ArrayList<String> results){}
	public ArrayList<NameValue> setHeaders(){return new ArrayList<NameValue>();}
	public void onRedirected(String location){}
	public void onError(Exception ex) {}	
	public DefaultHttpClient setDefaultHttpClient() {return null;}
	public String setAuthUsername() {return null;}
	public String setAuthPassword() {return null;}
	
	public void onDownloadProgressChange(long downloaded, long total) 
	{
		// This function will not be called when getStream/postStream function is used.
	}
		
	//adapter functionality	
	public final ArrayList<NameValue> getHeaders()
	{
		ArrayList<NameValue> list = new ArrayList<NameValue>();
		
		if(response != null)
		{
			Header[] headers = response.getAllHeaders();
			for(int i = 0; i < headers.length; i++)
				list.add(new NameValue(headers[i].getName(), headers[i].getValue()));
		}
		return list;
	}

	public final DefaultHttpClient getDefaultHttpClient()
	{
		return client;
	}
	@Override
	public boolean setHandleRedirect() {return true;}
}
